package com.producer;

import com.producer.entities.Measurement;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

public class Sender {

    public static void main(String[] argv) throws IOException, TimeoutException {

        //final String sensorId =  argv[0];
        final String sensorId = "fba77b5c-29db-49b3-afbd-6b56570d106b";
        System.out.println("The sensor id is: " + sensorId);

        String fileName = "sensor.csv";
        final BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

        final float lastValue = -1.0F;

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Running: " + new java.util.Date());

                final Measurement measurement = new Measurement();

                measurement.setSensorId(sensorId);
                measurement.setTimestamp(new Timestamp(System.currentTimeMillis()));

                String currentLine = "";

                try {
                    while(true) {
                        if((currentLine = bufferedReader.readLine()) == null || Float.parseFloat(currentLine) > lastValue) {
                            break;
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                measurement.setMeasurementValue(Float.parseFloat(currentLine));

                System.out.println(measurement.toString());

                ConnectionFactory factory = new ConnectionFactory();
                //factory.setHost("localhost");
                try {
                    factory.setUri("amqps://itixjdms:X0ob4mYK9o1l4ttXw7i9SMPrhAjJlk8g@kangaroo.rmq.cloudamqp.com/itixjdms");
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }

                try (Connection connection = factory.newConnection()) {
                    Channel channel = connection.createChannel();
                    channel.queueDeclare("hello-world", false, false, false, null);

                    channel.basicPublish("", "hello-world", false, null, measurement.toString().getBytes(StandardCharsets.UTF_8));

                    System.out.println("Message has been sent!");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 5000);
        // 60000 * 10
    }
}
