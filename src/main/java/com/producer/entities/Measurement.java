package com.producer.entities;

import java.sql.Timestamp;

public class Measurement {
    private Timestamp timestamp;
    private String SensorId;
    private float measurementValue;

    public Measurement(Timestamp timestamp, String sensorId, float measurementValue) {
        this.timestamp = timestamp;
        SensorId = sensorId;
        this.measurementValue = measurementValue;
    }

    public Measurement() {

    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getSensorId() {
        return SensorId;
    }

    public void setSensorId(String sensorId) {
        SensorId = sensorId;
    }

    public float getMeasurementValue() {
        return measurementValue;
    }

    public void setMeasurementValue(float measurementValue) {
        this.measurementValue = measurementValue;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "timestamp=" + timestamp +
                ", SensorId='" + SensorId + '\'' +
                ", measurementValue=" + measurementValue +
                '}';
    }
}
